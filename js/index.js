//**BASICS OF JAVASCRIPT

// console.time();
// window.alert("Demo project");
// console.log("Demo console check");
// console.table(
//     {
//         name : 'rahul',
//         class: 10,
//         section: 'D',
//         marks: 85
//     }
// )

// console.warn("This is a Warning");
// console.error(18>19,"This statement is false");
// console.timeEnd();
// console.assert(0>1, "This statement is false");
// console.error("this is a simple error");
// for(let i=0;i<100;i++)
// {
//     console.count(i);
// }


//**VARIABLE DECLARATION IN JAVASCRIPT

//var city=`delhi`;
//console.log(city);

// const city2="Mumbai";
// city2='Ahemdabad';  //assignment to constant variable not possible
// console.log(city2);

//let (Block level scope)

// let country="India";
// country ="australia";  //assignment is possible
// console.log(country);

// var age=54;
// console.log(age);

// {
//     let age =76;
//     console.log(age);
// }
// console.log(age);

// console.group('simple'); 
// console.log('Grouped'); 
// console.groupEnd('simple'); 
// console.log('new section');

// var i=50;
// console.log(i);

// {
//     let i=100;
//     console.log(i);
//     i=140;
//     console.log(i);

// }
// var i=150;
// console.log(i);

// const marks =70; //Initialization is important
// console.log(marks);

// {
//     const marks =50;
//     console.log(marks);
//     marks=60;
//     console.log(marks);  // 'marks' has already been declared
// }

//DATATYPE OF VARIABLE

// let variable = true;
// console.log(typeof variable);

// let myarr = [1, 2, 3, 4];
// console.log("Data type is " + (typeof myarr));

// const res='100';
// let y=Number(100);
// console.log(y);
// console.log(typeof y);

// let name='hello';
// let result = Number(name);
// console.log(result);

// let z=48;
// let t = z.toString();
// console.log(t);
// console.log(typeof t);

// DIFFERNCE OF TWO STRINGS

// let res='3'-'4';
// console.log(res);
// let z=10 - true;
// console.log(z);
// let f='4' - '3';
// console.log(f);

// stri = ([1,2,3,4,5,6,7,8,9]);
// console.log(stri , Array);

// let g = '10.5487923352123322';
// console.log(g.length);

// let g=10.22549649846565113;
// console.log(g.toFixed(5));

// var mystring = 'properties';
// console.log(mystring.charAt(2));

// var k = 'Javascrit';
// console.log(mystring +' '+ k); //String concat
// var z= mystring.concat(' '+k);
// console.log(z);

// var mystring = 'Indian Subcontinent';
// console.log(mystring.lastIndexOf('i'));

// var mystring = 'Indian Subcontinent';
// console.log(mystring.split('I'));

// var str = "Welcome to programming World!!";
// var pos = str.search("programming");
// console.log(pos);

// var txt = "abcdefgh";   // String
// console.log(txt.split(""));          // Split on commas
// txt.split(" ");          // Split on spaces

// let name='Rahul';
// let fruit1 = 'Orange\'';
// let fruit2 = 'Apple';
// let myHtml = `Hello ${name}
//             <h1> This is "my" heading </h1>
//             <p> You like ${fruit1} and ${fruit2}`;

// document.body.innerHTML = myHtml;  

// let arr = ['first element', 'second element', 'last element'];
// console.log(arr[0]);
// console.log(arr[arr.length - 1]); 


/*
1 DEC 2020
*/

// let fruits=['Apple','banana','strawberry'];
// let y = fruits.splice(0,1);
// console.log(fruits);

//let obj = new object();
// let obj = {
//     name : 'Rahul',
//     marks : 85,
//     class : 11,
//     age : 17,
//     'last name': 'Sharma'

// }

// console.log(obj['last name']);

 
// var person = {name: "Harry", language: "JavaScript", age: 20};  
// for(var i in person) {  
// console.log( i + " = " + person[i]); 
// }

// let i = 10;
// for (let x = 1; x <=i; x++) {
//     if (x == 5) {
//         continue;
//     }}

// let i=10;
// for(let k=0;k<=i;k++){
//     if(k==5)
//     {
//         continue;
//         console.log(k);
//     }
//     else{
//         console.log(k);
//     }
// }

// let k = 0;
// do {
//     if(k===5){
//         k +=1;
//         continue;
//     }
//   console.log(k + 1);
//   k +=1;
// } while (k < 10);


// let i=10;
// function ui(name)
// {
//     let i = 9;
//     console.log(i);
//     return `This is a ${name} ui`;
// }

// console.log(ui("harry"), i)

// const myobj = {
//         name: "SkillF",
//         game: function(){
//             return "GTA";
//         }
//     }
//     console.log(myobj.game());

// let a = document.body;

// Array.from(a).forEach(function(element){
//     console.log(element);
// });
 
//tut 14

// SINGLE ELEMENT SELECTOR 

//let element = document.getElementById('myfirst');
//console.log(element.parentElement);
//element.style.color='red';
//element.innerText = 'Rahul';
// element.innerHTML = '<b> RAHUL</b>'

//let c = document.querySelector('#myfirst');
//c= document.querySelector('.child');
//c=document.querySelector('div');
//c.style.color = 'blue';
//console.log(c);


// MULTI ELEMENT SELECTOR

// let element = document.getElementsByClassName('child');
// console.log(element[2]);

//let element =document.getElementsByClassName('child');
//console.log(element);

//element = document.getElementsByClassName('container');
//console.log(element[0]);

//console.log(element[0].getElementsByClassName('child'));

//element= document.getElementsByTagName('div');
//element1= document.getElementsByTagName('div');
//console.log(element);

// Array.from(element).forEach(element => {
//     element.style.color='green';
//     console.log(element);
// });

// for (let index = 0; index < element1.length; index++) {
//     const element = element1[index];
//     console.log(element);
// }

//***2 DEC20 */

/* <div class="add">
<h2 class="add__title">title</h2>
</div>
const component = document.querySelector('.add')
console.log(component)


const items= document.querySelector('.myclass')
const l_Items = items.children
console.log(l_Items)

const mylist = document.querySelectorAll('li')
const firstItem = mylist[0]
const secondItem = mylist[1]
console.log(firstItem)
console.log(secondItem)

const mylist = document.querySelectorAll('li')
const firstItem = mylist[0]
const secondItem = mylist[1]
console.log(firstItem.parentElement)
console.log(secondItem.parentElement)

const item1 = document.querySelector('li')
const item2 = item1.nextElementSibling
console.log(item2)

const item5 = document.querySelectorAll('li')[1]
const item6 = item5.previousElementSibling
console.log(item6)

var element= document.createElement("name");
let div = document.createElement('div');

var paragraph = document.createElement("P");                
var text = document.createTextNode("This is a paragraph.");  

var paragraph = document.createElement("P");                
var text = document.createTextNode("This is a paragraph.");       
paragraph.appendChild(text);  

document.getElementById("myAnchor").setAttribute("href", "https://codewithharry.com/");
var h = document.getElementById("myAnchor").getAttribute("target");

let result = element.hasAttribute(name);
var h = document.getElementById("Btn").hasAttribute("onclick");

document.getElementById("myAnchor").removeAttribute("href");
let element1 = document.getElementById("myid1 ");
let element2 = document.createElement("u");
let content = document.createTextNode("Added Content");
element2.appendChild(content); 
element1.replaceWith(element2);

parentDiv.replaceChild(sp1, sp2);

let list = document.getElementById("myList");
 if (list.hasChildNodes()) {
 list.removeChild(list.childNodes[1]);
 }

 <input value="Click here" onclick="alert('Click here!')" type="button"></input>
 element.addEventListener(event, function, useCapture);

 document.addEventListener("click", function(){ alert("Event Occurred"); });

document.addEventListener("click", myfunc);
 function myfunc () {
 document.getElementById("demo").innerHTML = "Hello World";
 }

 document.addEventListener("click", myfunc);
function myfunc(event) {
alert(event.type + " at " + event.currentTarget);
alert("Coordinates: " + event.clientX + ":" + event.clientY);};

const myclick = document.querySelector('aside');
myclick.addEventListener('dblclick', function (e) {
});

const test= document.getElementById('test');
test.addEventListener('mousemove', function (e) {
});

const test= document.getElementById('test');
test.addEventListener("mouseover", function( event ) { event.target.style.color = "red";});

const test= document.getElementById('test');
test.addEventListener("mouseout", function( event ) {   
event.target.style.color = "red";});

const mouseTarget = document.getElementById('mouseTarget');
mouseTarget.addEventListener('mouseenter', function(e) {
mouseTarget.style.border = '5px dotted blue';});

const mouseTarget = document.getElementById('mouseTarget');
mouseTarget.addEventListener('mouseleave', function(e){
mouseTarget.style.border = '1px solid red'; });

myevent.addEventListener('mousedown', function(e) {
    console.log("Mousedown event occur")});

    myevent.addEventListener('mouseup', function(e) {
        console.log("Mouseup event occur")}); */

//*****************

// let cont = document.querySelector('.no');
// cont = document.querySelector('.container');
//console.log(cont.children);
// let nodeName = cont.childNodes[1].nodeName;
// let nodeType = cont.childNodes[0].nodeType;

// console.log(nodeName);
// console.log(nodeType);

/*
nodeTypes

1. element
2.attribute
3.textnode
8.comment
9.document
10.docType

*/
//console.log(cont);

// let container = document.querySelector('.container');
// console.log(container);
// console.log(container.children[1].children[0].children);

//console.log(container.firstElementChild);
// console.log(container.firstChild);

// console.log(container.lastElementChild);
// console.log(container.lastChild);

// console.log(container.childElementCount);

//console.log(container.firstElementChild.parentNode);

//console.log(container.firstElementChild.nextSibling);
//console.log(container.firstElementChild.nextElementSibling);

// let element = document.createElement('li');
// element.className='childul';
// element.setAttribute('title','mytitle');
// element.id='createdLi';

// element.innerText='Created by ME';
// element.innerHTML=`<b>created using innerHTML method</b>`;
// let text=document.createTextNode('Text created using createTextNode');
// element.appendChild(text);



// let ul =document.querySelector('ul.this');
// ul.appendChild(element);
// console.log(ul);
// console.log(element);

// //Use of replaceWith,replaceChild and removeChild 

// let myul = document.getElementById('myul');
// myul.replaceChild(element,document.getElementById('fui'));
// console.log(myul);

// myul.removeChild(document.getElementById('lui'));
// let pr= element.getAttribute('id');
// pr=element.removeAttribute('id');
// console.log(pr);
 
// exercise

// let elem = document.createElement('a');
// elem.setAttribute('id','elem');
// console.log(elem);
// elem.setAttribute('href','https://codewithharry.com/')

// let elem2 = document.createElement('h6');
// elem2.innerText='Go to CodeWithHarry';
// elem2.setAttribute('id','elem2');
// console.log(elem2);

// let appendElement = document.getElementById('heading');
// appendElement.appendChild(elem);



// let appendElement2= document.getElementById('elem');
// appendElement2.appendChild(elem2);
// console.log(appendElement);

//document.getElementById("elem2").setAttribute("href", "https://codewithharry.com/");

//*****7 DEC20 */

// document.getElementById('heading').addEventListener('click',function clickevent(e){
//     console.log('You have clicked the heading');
//     let variable = e.target;
//    variable=e.target.className;
//     console.log(variable);

// });

//project**(Demo)
//**temporary code */

// console.log("Welcome s");
// showNotes();

// // If user adds a note, add it to the localStorage
// let addBtn = document.getElementById("addBtn");
// addBtn.addEventListener("click", function(e) {
//   let addTxt = document.getElementById("addTxt");
//   let notes = localStorage.getItem("notes");
//   if (notes == null) {
//     notesObj = [];
//   } else {
//     notesObj = JSON.parse(notes);
//   }
//   notesObj.push(addTxt.value);
//   localStorage.setItem("notes", JSON.stringify(notesObj));
//   addTxt.value = "";
// //   console.log(notesObj);
//   showNotes();
// });

// // Function to show elements from localStorage
// function showNotes() {
//   let notes = localStorage.getItem("notes");
//   if (notes == null) {
//     notesObj = [];
//   } else {
//     notesObj = JSON.parse(notes);
//   }
//   let html = "";
//   notesObj.forEach(function(element, index) {
//     html += `
//             <div class="noteCard my-2 mx-2 card" style="width: 18rem;">
//                     <div class="card-body">
//                         <h5 class="card-title">Note ${index + 1}</h5>
//                         <p class="card-text"> ${element}</p>
//                         <button id="${index}"onclick="deleteNote(this.id)" class="btn btn-primary">Delete Note</button>
//                     </div>
//                 </div>`;
//   });
//   let notesElm = document.getElementById("notes");
//   if (notesObj.length != 0) {
//     notesElm.innerHTML = html;
//   } else {
//     notesElm.innerHTML = `Not Available "Add a Note" .`;
//   }
// }

// // Function to delete a note
// function deleteNote(index) {
// //   console.log("I am deleting", index);

//   let notes = localStorage.getItem("notes");
//   if (notes == null) {
//     notesObj = [];
//   } else {
//     notesObj = JSON.parse(notes);
//   }

//   notesObj.splice(index, 1);
//   localStorage.setItem("notes", JSON.stringify(notesObj));
//   showNotes();
// }


// let search = document.getElementById('searchTxt');
// search.addEventListener("input", function(){

//     let inputVal = search.value.toLowerCase();
//     // console.log('Input event fired!', inputVal);
//     let noteCards = document.getElementsByClassName('noteCard');
//     Array.from(noteCards).forEach(function(element){
//         let cardTxt = element.getElementsByTagName("p")[0].innerText;
//         if(cardTxt.includes(inputVal)){
//             element.style.display = "block";
//         }
//         else{
//             element.style.display = "none";
//         }
//         // console.log(cardTxt);
//     })
// })

